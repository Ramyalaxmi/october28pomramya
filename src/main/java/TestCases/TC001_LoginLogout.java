package TestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_LoginLogout extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "Login and Click CRMSFA";
		testDesc = "Login and navigate to My Home page";
		author = "Ramya";
		category = "smoke";
		dataSheetName = "TC001";
	}
	
	
	
	@Test(dataProvider = "fetchData")
	public void loginLogOut(String uName, String password)
	{
		
		new LoginPage().enterUserName(uName).enterPassword(password).clickLogin().clickCRM();
		
		
		
		
		
	}
	
	
	

}
