package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how = How.ID, using = "createLeadForm_companyName") WebElement eleCName;
	@FindBy(how = How.ID, using = "createLeadForm_firstName") WebElement eleFName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName") WebElement eleLName;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement eleClickCreateLeadSubmit;
	
	public CreateLead enterCompanyName(String cName)
	{
		type(eleCName, cName);
		return this;
	}
	
	public CreateLead enterFirstName(String fName)
	{
		type(eleFName, fName);
		return this;
	}
	
	public CreateLead enterLastName(String lName)
	{
		type(eleLName, lName);
		return this;
	}
	
	
	
	
	public ViewLeads ClickCreateLeadSubmit()
	{
		click(eleClickCreateLeadSubmit);
		return new ViewLeads();
	}
	

}
