package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeads extends ProjectMethods{

	public ViewLeads()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "viewLead_firstName_sp") WebElement eleVerifyFName;
	
	public ViewLeads VerifyFirstName(String fName)
	{
		
		String text = eleVerifyFName.getText();
		System.out.println("First name in View Leads "+text);
		return this;
	}

	
	
	
	
}
