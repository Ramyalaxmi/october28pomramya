package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import LearnExcel.learnExcelintegration;



public class ProjectMethods extends SeMethods{
	
	public String dataSheetName;
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("@BeforeTest");
	}
	@BeforeClass
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}
	@Parameters({"url"})
	@BeforeMethod
	public void login(String url) {
		startNode();
		startApp("chrome", url);
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crm = locateElement("linktext", "CRM/SFA");
		click(crm);*/		
	}
	
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	@AfterClass
	public void afterClass() {
		System.out.println("@AfterClass");
	}
	@AfterTest
	public void afterTest() {
		System.out.println("@AfterTest");
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name = "fetchData")
		public Object[][] getData() throws IOException
		{
			learnExcelintegration excel = new learnExcelintegration();
			Object[][] sheet = excel.LearnExcel(dataSheetName);
			return sheet;
		}


	
	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}

}






